# Weather Link

ASP.NET Core web services for weather based information.

See https://www.adamweiss.me/development/weatherlink/ for information about this project.

See https://localhost:5001/ while developing for the endpoints.
